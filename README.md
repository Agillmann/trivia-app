# Trivia Web App

Quiz utilisant l'api [jService](http://jservice.io).

[Demo](https://trivia-app.netlify.com)

## Regles du jeux

Pour finir le jeu vous devez répondre a 10 catégories, avec un minimum de 3 bonne réponses par catégorie.

### Prérequis

Pour pouvoir installer le projet sur votre machine il vous faut :

* Node.js

* npm ou yarn

* React

  * react

  * react-dom

### Installation

* Installation de node.js

```
$ sudo apt-get install nodejs npm
```

* Installation de yarn

  * Configure the repository

  ```
  $ curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

  $ echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
  ```

  * Installation

  ```
  $ sudo apt-get update && sudo apt-get install yarn
  ```
  
* Récupérer le projet 

```
$ git clone https://gitlab.com/Agillmann/trivia-app quiz
```

* Deploiment de l'application

  * Avec npm
  ```
  $ npm install

  $ npm start
  ```

  * Avec yarn
  ```
  $ yarn

  $ yarn start
  ```

L'application sera disponible a l'adresse : [localhost:3000](localhost:3000)

## Auteurs

* **Adrien Gillmann** - *Maintainer* - [Gitlab](https://gitlab.com/Agillmann)
